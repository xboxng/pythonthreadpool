import time
import logging
from threadpoolexecutor import ThreadPoolExecutor

logging.basicConfig()
exe = ThreadPoolExecutor(10)


def throw_err(v):
    def f():
        raise ValueError(v)
    return f


def print_msg(v):
    def f():
        time.sleep(5)
        print('haha %d' % v)
        return v
    return f

fs = []

for i in range(5):
    f = exe.submit(throw_err(i))
    fs.append(f)

for i in range(5):
    f = exe.submit(print_msg(i))
    fs.append(f)
exe.start()
print "waiting for f.get()"
for f in fs:
    try:
        print(f.get())
    except ValueError, e:
        print("value error")
    finally:
        pass
print "f.get() returned"
