# Copyright 2012 Qiang Yu

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import time
import threading
import logging

from Queue import Queue
from threading import Thread

logger = logging.getLogger('threadpoolexecutor')
logger.setLevel(logging.INFO)


class Future(object):
    '''
    please check Java Future class
    '''
    def __init__(self):
        self._queue = Queue(1)

    def get(self, block=True, timeout_in_seconds=None):
        result = self._queue.get(block, timeout_in_seconds)
        if isinstance(result, _TaskExceptionWrapper):
            raise result.exception
        return result

    def _put(self, anything):
        self._queue.put(anything)


class _TaskWrapper():
    '''
    this class is a wrapper of submitted tasks. it has a future object which
    can be checked to see if the contained task is finished or not
    '''
    def __init__(self, task):
        self._task = task
        self._future = Future()

    def __call__(self):
        try:
            result = self._task()
        except Exception, e:
            result = _TaskExceptionWrapper(e)
        finally:
            self._future._put(result)

    @property
    def future(self):
        return self._future


class _TaskExceptionWrapper(object):
    '''
    this class wraps exceptions thrown by thread tasks
    '''
    def __init__(self, e):
        self._exception = e

    @property
    def exception(self):
        return self._exception


class _Worker(Thread):
    '''
    threads living in the pool
    '''
    def __init__(self):
        super(_Worker, self).__init__()
        self._task = None
        self._shutdown_event = threading.Event()
        self._task_assigned_event = threading.Event()

    @property
    def is_idle(self):
        return not self._task_assigned_event.is_set()

    def assign_task(self, task):
            self._task = task
            self._task_assigned_event.set()

    def run(self):
        while not self._shutdown_event.is_set():
            self._task_assigned_event.wait()
            if self._task:
                try:
                    self._task()
                finally:
                    self._task = None
                    self._task_assigned_event.clear()

    def join(self, timeout=None):
        # in case the worker is waiting for tasks
        self._task_assigned_event.set()
        self._shutdown_event.set()
        super(_Worker, self).join(timeout)


class ThreadPoolExecutor(Thread):
    def __init__(self, core_pool_size=1, maximum_pool_size=0):
        super(ThreadPoolExecutor, self).__init__()
        self._task_queue = Queue(0)
        self._pool = []
        self._core_pool_size = core_pool_size
        self._maximum_pool_size = maximum_pool_size
        self._inc_pool_size()
        self._shutdown_event = threading.Event()

    def _process_tasks_in_queue(self):
        work_scheduled = False
        task = None

        if not self._task_queue.empty():
            worker = None
            # queue not empty, find a worker to execute the task
            for w in self._pool:
                if w.is_idle:
                    worker = w
                    break
            if not worker:
                logger.debug('no idle worker')
                self._inc_pool_size()
            else:
                logger.debug('found an idle worker to run task')
                task = self._task_queue.get(False)
                worker.assign_task(task)
                if not worker.is_alive():
                    worker.start()
                work_scheduled = True
        else:
            # it might be better not to decrease pool size immediately
            self._dec_pool_size()
        return work_scheduled

    def run(self):
        while not self._shutdown_event.is_set() or not self._task_queue.empty():
            if not self._process_tasks_in_queue():
                # no tasks can be scheduled. sleep
                time.sleep(0.1)

        for w in self._pool:
            if w.is_alive():
                w.join()

    # adding threads into the thread pool
    def _inc_pool_size(self):
        # logger.debug('to increase thread pool size')
        while len(self._pool) < self._core_pool_size:
            self._pool.append(_Worker())

        if len(self._pool) < self._maximum_pool_size:
            self._pool.append(_Worker())

    # removing idle threads from the thread pool
    def _dec_pool_size(self):
        # logger.debug('to decrease pool size')
        if len(self._pool) > self._core_pool_size:
            for w in self._pool:
                if w.is_idle:
                    # only remove one worker
                    w.join(0.1)
                    self._pool.remove(w)
                    break

    def submit(self, task):
        # stop accepting new tasks if shutdown has been called
        if not self._shutdown_event.is_set():
            taskWrapper = _TaskWrapper(task)
            self._task_queue.put(taskWrapper)
            return taskWrapper.future

    def shutdown(self):
        self._shutdown_event.set()

    def join(self, timeout=None):
        self.shutdown()
        super(ThreadPoolExecutor, self).join(timeout)
